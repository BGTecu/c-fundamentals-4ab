# README #

C# Fundamentals for Absolute Beginners

### What is this repository for? ###

* [Microsoft Virtual Academy] (https://mva.microsoft.com/en-US/training-courses/c-fundamentals-for-absolute-beginners-16169?l=gr5buWQIC_206218949)
* Version 2017.07.10 - 18
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Quick projects summary ###

* HelloWorld
* Variables
* Decisions
* ForItineration
* UnderstandingArrays
* SimpleMethod
* HelperMethods
* WhileIteration
* WorkingWithStrings
* DatesAndTimes
* SimpleClasses
* ObjectLifeTime
* Lection18, MyClient, and MyCodeLibrary
* WorkingWithCollections
* UnderstandingLINQ
* HandlingExceptions
* TimerExample

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
