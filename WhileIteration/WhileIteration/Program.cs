﻿using System;
using System.Data.SqlTypes;
using System.Runtime.CompilerServices;

namespace WhileIteration
{
    class Program
    {
        static void Main(string[] args)
        {
            bool displayMenu = true;
            while (displayMenu)
            {
                displayMenu = MainMenu();
            }
        }


        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("1) Print Numbers");
            Console.WriteLine("2) Guessing Game");
            Console.WriteLine("3) Exit");
            Console.WriteLine("----------------");
            Console.Write("Chouse an option: ");

            string result = Console.ReadLine();

            if (result == "1")
            {
                PrintNumbers();
                return true;
            }
            else if (result == "2")
            {
                GuessingGame();
                return true;
            }
            else if (result == "3")
            {
                Console.Clear();
                Console.WriteLine("Aplication closed!");
                return false;
            }
            else
            {
                Console.WriteLine("Wrong option!");
                return true;  
            } 
        }

        private static void PrintNumbers()
        {
            Console.Clear();
            Console.WriteLine("Print Numbers!");
            Console.Write("Write a number: ");
            int result = int.Parse(Console.ReadLine());
            int counter = 1;

            while (counter <= result)
            {
                Console.Write(counter);
                if (counter < result)
                    Console.Write(" - ");
                counter++;
            }
            
            Console.WriteLine();
            Console.Write("Press [ENTER] to continue!");
            Console.ReadLine();
        }

        private static void GuessingGame()
        {
            Console.Clear();
            Console.WriteLine("Guessing Game!");
            
            Random myRandom = new Random();
            int randomNumber = myRandom.Next(1, 11);

            int guesses = 0;
            bool incorrect = true;

            do
            {
                Console.Write("Guess a number between 1 and 10: ");
                string result = Console.ReadLine();
                guesses++;

                if (result == randomNumber.ToString())
                    incorrect = false;
                else
                {
                    Console.Clear();
                    Console.Write("Wrong! ");
                }
                
            } while (incorrect);
            
            Console.WriteLine("Correct! It took you {0} guesses.", guesses);
            Console.WriteLine();
            Console.Write("Press [ENTER] to continue!");

            Console.ReadLine();
        }

    }
}